<?php
/**
 * Created by PhpStorm.
 * User: Veeraj Shenoy
 * Date: 31-12-2016
 * Time: 05:49 PM
 */
$whitelist = array(
    '127.0.0.1',
    '::1'
);

if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
    define("SERVERNAME", "localhost");
    define("USERNAME", "root");
    define("PASSWORD", "");
    define("DBNAME", "admin_panel");
}else{
    define("SERVERNAME", "localhost");
    define("USERNAME", "");
    define("PASSWORD", "");
    define("DBNAME", "admin_panel");
}