<?php
/**
 * Created by PhpStorm.
 * User: Veeraj Shenoy
 * Date: 11-03-2017
 * Time: 10:43 AM
 */
include "header.php";
?>
    <div id="page-wrapper" style="height: 100vh;">
        <div class="container well" style="margin-top: 10%">
            <h3 style="text-align: center"><i class="fa fa-user" aria-hidden="true"></i> Log In</h3>
            <hr>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email/Username:</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" placeholder="Enter email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Password:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label><input type="checkbox"> Remember me</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in" aria-hidden="true"></i>  Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
