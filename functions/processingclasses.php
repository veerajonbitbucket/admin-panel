<?php
/**
 * Created by PhpStorm.
 * User: Veeraj Shenoy
 * Date: 26-12-2016
 * Time: 10:41 PM
 */

//Processing classed will be present here

class encryptdecrypt{

    //Encrypt the password string to a hashed value using a key
    //Note for our project we will be using a 4 digit number
    function encrypt($string, $key){
        $iv = mcrypt_create_iv(
            mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
            MCRYPT_DEV_URANDOM
        );

        return $encrypted = base64_encode(
            $iv .
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $key, true),
                $string,
                MCRYPT_MODE_CBC,
                $iv
            )
        );
    }

    //Decrytps the hashed string back to password string provided we input the same key
    //Note for our project we will be using a 4 digit number
    function decrypt($encrypted, $key){
        $data = base64_decode($encrypted);
        $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

        return $decrypted = rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $key, true),
                substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                MCRYPT_MODE_CBC,
                $iv
            ),
            "\0"
        );
    }
}

class hashing{
    function generateHashWithSalt($password) {
        $intermediateSalt = md5(uniqid(rand(), true));
        $salt = substr($intermediateSalt, 0, MAX_LENGTH);
        return hash("sha256", $password . $salt);
    }

    function generateHash($password) {
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
            return crypt($password, $salt);
        }
    }

    function checkhash($user_input, $hashed_password){
        if (hash_equals($hashed_password, crypt($user_input, $hashed_password))) {
            return true;
        }else{
            return false;
        }
    }

}


class password{

    function newpassword($email, $companyname, $empname, $adminid, $six_digit_random_number){
        $encry = new encryptdecrypt();
        $encrytext = $encry->encrypt($email, $six_digit_random_number);

        $subject = "Invitation to join our organization";
        $message = "
        Hi ".$empname.",<br><br>
        
        Welcome to ".$companyname."! You have been invited to join the Organization ".$companyname." by the Administrator (".$adminid.").<br> 
        Please click <a href='http://www.paypeoples.com/resetpassword.php?email=".$email."'>here</a> to accept the invitation and join your organization.<br>       
        If this email is not meant for you, please ignore.<br><br><br>
        
        Your Code is <b>".$six_digit_random_number."</b><br><br><br>
        
        Thank You,<br>
        ".$companyname;

        $mail = new mail();
        $mail->sendmail($email, $adminid, $subject, $message);
    }

    function resetpassword($email){
        $adminid = 'admin@paypeoples.com';
        $encry = new encryptdecrypt();
        $six_digit_random_number = $six_digit_random_number = mt_rand(100000, 999999);
        $encrytext = $encry->encrypt($email, $six_digit_random_number);

        $subject = "Password Reset!";
        $message = "
        Please click <a href='http://www.paypeoples.com/resetpassword/".$six_digit_random_number."/".urlencode($encrytext)."'>here</a> to reset your password.<br>  
             or type $six_digit_random_number in your code box<br>
        If this email is not meant for you, please ignore.<br><br><br>
        
        Thank You,<br>
        Team Paypeoples
        ";

        $mail = new mail();
        $mail->sendmail($email, $adminid, $subject, $message);
        return $six_digit_random_number;
    }
}

class mail{
    function sendmail($to, $from, $subject, $message){

// Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
        $headers .= 'From: <'.$from.'>' . "\r\n";

        mail($to,$subject,$message,$headers);
    }
}



//Returns array of date betweeen two given date value
//print_r( returnBetweenDates( '12-05-2016', '26-05-2016' ) );
function returnBetweenDates( $startDate, $endDate ){
    $startStamp = strtotime(  $startDate );
    $endStamp   = strtotime(  $endDate );
    if( $endStamp > $startStamp ){
        while( $endStamp >= $startStamp ){
            $dateArr[] = date( 'd-m-Y', $startStamp );
            $startStamp = strtotime( ' +1 day ', $startStamp );
        }
        return $dateArr;
    }else{
        return [$startDate];
    }
}


function listofallemployees($company_name, $conn){
    $sql = "SELECT * FROM `".$company_name."`.employeemaster WHERE is_delete IS NULL";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $emp[] = "'".$row["Ename"]."'";
    }
    if(isset($emp)){
        return "[".implode(",",$emp)."]";
    }else{
        return "[]";
    }
}
